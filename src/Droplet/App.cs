using System;
using System.Threading;
using Microsoft.SPOT;
using Netduino.Foundation.Sensors.Rotary;
using H = Microsoft.SPOT.Hardware;
using N = SecretLabs.NETMF.Hardware.NetduinoPlus;

namespace Droplet
{
    public class App
    {
        // All times in milliseconds
        private const int CameraActuationDelay = 80;
        private const int MinTimeBetweenTriggers = 2500;
        private const int TimingIncrement = 5;
        private const int LongPressThreshold = 500;
        private const int WaterValveOpenTime = 10000;
        private const int MultiPressPrevent = 2000;

        private H.OutputPort _led;
        private H.OutputPort _resetLed;
        private H.OutputPort _dropper;
        private H.OutputPort _camera;
        private H.InterruptPort _button;
        private H.InterruptPort _onboardButton;
        private RotaryEncoder[] _rotary;

        private int[] DefaultTimings = { 25, 100, 50, 250 };
        private int[] CurrentTimings = new int[4];

        enum Knob : int
        {
            Undefined = -1,
            Drop1Size = 0,
            DropGap = 1,
            Drop2Size = 2,
            CameraDelay = 3
        }

        enum TimingConstraints : int
        {
            Min = 10,
            Max = 500
        }

        private static bool _inProc = false;
        private static DateTime _pressDateTime;
        private static DateTime _interruptDateTime = DateTime.Now;
        private static DateTime _lastTrigger = DateTime.Now;

        public App()
        {
            // Set current timings to default
            RestoreTimings();
            // Initialize all peripherals and event handlers
            InitializePeripherals();
        }

        private void InitializePeripherals()
        {
            _led = new H.OutputPort(N.Pins.GPIO_PIN_D13, false);
            _resetLed = new H.OutputPort(N.Pins.ONBOARD_LED, false);
            _dropper = new H.OutputPort(N.Pins.GPIO_PIN_D11, true);
            _camera = new H.OutputPort(N.Pins.GPIO_PIN_D10, true);
            _button = new H.InterruptPort(N.Pins.GPIO_PIN_D12, false, H.Port.ResistorMode.PullUp, H.Port.InterruptMode.InterruptEdgeLow);
            _onboardButton = new H.InterruptPort(N.Pins.ONBOARD_SW1, false, H.Port.ResistorMode.Disabled, H.Port.InterruptMode.InterruptEdgeBoth);

            _rotary = new RotaryEncoder[]
            {
                new RotaryEncoder(N.Pins.GPIO_PIN_D1, N.Pins.GPIO_PIN_D0),
                new RotaryEncoder(N.Pins.GPIO_PIN_D3, N.Pins.GPIO_PIN_D2),
                new RotaryEncoder(N.Pins.GPIO_PIN_D5, N.Pins.GPIO_PIN_D4),
                new RotaryEncoder(N.Pins.GPIO_PIN_D7, N.Pins.GPIO_PIN_D6)
            };

            // Turn leds off at start
            _resetLed.Write(false);
            _led.Write(false);

            // add an event-handler for the switches. Init and Reset/Valve
            _button.OnInterrupt += new H.NativeEventHandler(Button_OnInterrupt);
            _onboardButton.OnInterrupt += new H.NativeEventHandler(OnBoardButton_OnInterrupt);

            // timing knobs, rotary encoders event-handlers
            foreach (var knob in _rotary)
            {
                knob.Rotated += Knob_Rotated;
            }
        }

        /// <summary>
        /// Restore all timings to the default parameters
        /// </summary>
        private void RestoreTimings()
        {
            Array.Copy(DefaultTimings, CurrentTimings, 4);
        }

        /// <summary>
        /// Button Press Event, starts entire process
        /// </summary>
        /// <param name="port">Port</param>
        /// <param name="data">Data</param>
        /// <param name="time">Timestamp</param>
        private void Button_OnInterrupt(uint port, uint data, DateTime time)
        {
            // Prevent multiple simultaneous actions...
            if (_inProc || _lastTrigger.AddMilliseconds(MinTimeBetweenTriggers) > time)
                return;

            _inProc = true;
            _led.Write(true);

            // Start camera delay sequence on a separate thread.
            // Sequential order triggers the camera too late.
            Thread thread = new Thread(() =>
            {
                Thread.Sleep(CurrentTimings[(int)Knob.CameraDelay]);
                _camera.Write(false); // Trigger Camera
                Thread.Sleep(CameraActuationDelay);
                _camera.Write(true); // Stop Trigger Camera
            });
            thread.Start();

            // Initiate Drop # 1
            _dropper.Write(false); // Open Valve
            Thread.Sleep(CurrentTimings[(int)Knob.Drop1Size]);
            _dropper.Write(true); // Close Valve, release drop

            Thread.Sleep(CurrentTimings[(int)Knob.DropGap]); // Time of flight Drop #1

            // Initiate Drop # 2
            _dropper.Write(false); // Open Valve
            Thread.Sleep(CurrentTimings[(int)Knob.Drop2Size]);
            _dropper.Write(true); // Close Valve, release drop

            Thread.Sleep(MinTimeBetweenTriggers);   // Pause before next cycle

            _led.Write(false);
            _lastTrigger = DateTime.Now;
            _inProc = false;
        }

        /// <summary>
        /// Short Press: Resets timming to its default state
        /// Long Press: Allows to empty the water present on the line
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnBoardButton_OnInterrupt(uint port, uint data, DateTime time)
        {
            // will be 1 when pressed (raised high), and 0, when unpressed
            if (data == 1)
            {
                _pressDateTime = time;
                return;
            }

            // Simple debounce filter...
            // Prevent repeted execution within threshold
            if (_interruptDateTime.AddMilliseconds(MultiPressPrevent) > time)
                return;

            // Short Press
            if (_pressDateTime.AddMilliseconds(LongPressThreshold) > time)
            {
                _resetLed.Write(true);
                RestoreTimings();
                Thread.Sleep(250);
                _resetLed.Write(false);
            }
            else // Long Press
            {
                _resetLed.Write(true);
                _dropper.Write(false); // Open Valve
                Thread.Sleep(WaterValveOpenTime);
                _dropper.Write(true); // Close Valve
                _resetLed.Write(false);
            }

            _interruptDateTime = time;
        }

        /// <summary>
        /// Rotary Encoder event handler
        /// Knobs that control timings
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Knob_Rotated(object sender, RotaryTurnedEventArgs e)
        {
            var encoder = sender as RotaryEncoder;
            if (encoder != null)
            {
                // determine offset based on the rotation. clockwise -> positive, counter-clockwise -> negative
                int timingOffset = (e.Direction == RotationDirection.Clockwise)
                    ? TimingIncrement
                    : TimingIncrement * (-1);

                Knob knob;
                var pin = (int)(encoder.APhasePin as H.Port).Id;
                switch (pin)
                {
                    case 38:
                        knob = Knob.Drop1Size;
                        break;
                    case 2:
                        knob = Knob.DropGap;
                        break;
                    case 24:
                        knob = Knob.Drop2Size;
                        break;
                    case 1:
                        knob = Knob.CameraDelay;
                        break;
                    default:
                        knob = Knob.Undefined;
                        break;
                }

                // make sure we mapped the interrupt to a valid knob
                if (knob != Knob.Undefined)
                {
                    // make sure the timing offset is within bounds
                    var newValue = CurrentTimings[(int)knob] + timingOffset;
                    if ((int)TimingConstraints.Min <= newValue && newValue <= (int)TimingConstraints.Max)
                    {
                        CurrentTimings[(int)knob] = newValue;
                    }
                }

            }
        }
        public void Run()
        {
            Debug.Print("Welcome to Droplet Photography Coolnes!");
            // wait for interrupts...
        }

    }
}
