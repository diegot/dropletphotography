using System.Threading;

namespace Droplet
{
    public class Program
    {
        public static void Main()
        {
            var app = new App();
            app.Run();

            Thread.Sleep(Timeout.Infinite);
        }
    }
}
